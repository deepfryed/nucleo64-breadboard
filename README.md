# Nucleo 64 Breadboard Breakout / Adapter

Nucleo-64 breadboard adapter, inspired by https://www.reddit.com/r/stm32f4/comments/gp7j5x/stm32_nucleo64_breadboard_adapter_feedback_wanted/

## Board

### Top
![](nucleo64-bb-top.png)


### Bottom
![](nucleo64-bb-bottom.png)

### Tools

* Kicad - https://www.kicad.org
* Rounded traces plugin - https://mitxela.com/projects/melting_kicad

### License

CERN OHL-P

https://ohwr.org/cern_ohl_p_v2.txt
